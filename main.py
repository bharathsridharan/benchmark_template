import yaml

try:
    with open("params.yaml","r") as file:
        data = yaml.load(file,Loader=yaml.FullLoader)
            
except FileNotFoundError:
    print("File Path/Name incorrect, check the path")

    
for k,v in data.items():
    print(k,":",v)
    
assert 'model_params' in data.keys()
print(data.get('model_params'))

        
