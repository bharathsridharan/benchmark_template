from imageio import imread, imwrite
import random
import os, argparse
import math
import numpy as np
import cv2
from math import floor

class MDataGenerator(tf.keras.utils.Sequence):
    '''
    Data Generator which would take random crop of the imagenet and yield as numpy array

    Args:
        mode: train or validation data 
        folder: base path of the imagenet 
        batch_size: size of the mini batch 
        perc_data: how much percent data to be yielded

    Attributes:
        mode: string - train.txt or val.txt
        folder: string - ILSVRC
        batch_size: interger 
        perc_data: float 
    '''
    def __init__(self, mode, folder, batch_size, perc_data = 1):
        self.folder = folder
        self.batch_size = batch_size
        self.current = 0
        self.mode = mode

        dataset = [l.split(" ") for l in open(self.folder+self.mode,"r").readlines()]
        dataset = [ [self.folder + "/" + d[0], int(d[1])] for d in dataset ]#[:2000]
        if perc_data < 1:
            dataset = dataset[:len(dataset)*perc_data]
        self.imgs_path, self.labels = zip(*dataset)

    def __len__(self):
        return math.ceil(len(self.labels)/ self.batch_size)

    def __getitem__(self, idx):
        i = idx*self.batch_size
        ii = min(i+self.batch_size, len(self.labels))
        return maps_images_cv(self.imgs_path[i:ii]), np.asarray(self.labels[i:ii])

def load_img(path_img):
    #read image
    img = imread(path_img)
    if len(img.shape) <= 2:
        img = cv2.merge((img,img,img))
    else:
        if img.shape[2]==1:
            c = img[...,0]
            img = cv2.merge((c,c,c))
        if img.shape[2]>3:
            img = img[...,0:3]

    if len(img.shape) == 2:
        print(img.shape)
    #resize so that the smaller dimension is 512
    mmin = min(img.shape[0], img.shape[1])
    scale = 512./mmin
    new_shape = [floor(img.shape[0]*scale), floor(img.shape[1]*scale)]
    return cv2.resize(img, tuple(new_shape[::-1]))


def maps_image_cv(img):
    final_size = (224,224)
    img = load_img(img)
    scale = random.random()/2.+ 0.5
    new_scale = img.shape
    new_scale = (int(round(scale*new_scale[0])), int(round(scale*new_scale[1])))
    img2 = cv2.resize(img, tuple(new_scale[::-1]))
    dx, dy = random.randrange(0,new_scale[0]-final_size[0]), random.randrange(0,new_scale[1]-final_size[1])
    return img2[dx:final_size[0]+dx,dy:final_size[1]+dy,:]


def maps_images_cv(imgs):
    return np.asarray([ maps_image_cv(img) for img in imgs ])
