import tensorflow as tf
import upstride.type2 as up
from upstride.cnn_modules import *

def alexnet(pretrained_weights = None, input_size = (224,224,3)):
    
    with tf.device('/device:CPU:0'):
        inputs = tf.keras.layers.Input(input_size)
        # TF to upstride
        x = up.TF2Upstride()(inputs)
        # Layer 1 - Conv
        x = up.Conv2D(96//factor, (11,11), 4, padding='same')(x)
        x = up.BatchNormalization()(x)
        x = tf.keras.layers.Activation('relu')(x)
        x = up.MaxPooling2D((3,3), strides=(2,2))(x)
        # Layer 2 - Conv
        x = up.Conv2D(256//factor, (5,5), padding='same')(x)
        x = up.BatchNormalization()(x)
        x = tf.keras.layers.Activation('relu')(x)
        x = up.MaxPooling2D((3,3), strides=(2,2))(x)
        # Layer 3 - Conv
        x = up.Conv2D(384//factor, (3,3), padding='same', activation='relu')(x)
        # Layer 4 - Conv
        x = up.Conv2D(384//factor, (3,3), padding='same', activation='relu')(x)
        # Layer 5 - Conv
        x = up.Conv2D(256//factor, (3,3), padding='same', activation='relu')(x)
        # Layer 6 - Fully connected
        x = tf.keras.layers.Flatten()(x)
        x = up.Dense(4096//factor, activation="relu")(x)
        # Layer 7 - Fully connected
        x = up.Dense(4096//factor, activation="relu")(x)
        
        x = up.Dense(1000)(x)
        
        # Upstride to TF
        x = up.Upstride2TF()(x)

        soft = tf.keras.layers.Activation("softmax")(x)

        model = tf.keras.models.Model(inputs, soft)

        model.summary()

        if(pretrained_weights):
            model.load_weights(pretrained_weights)

    return model
